# README #

### How do I get set up? ###

* First clone the repository ( git clone https://sorinpnt@bitbucket.org/sorinpnt/david-e.git )
* Install npm dependencies with a simple `npm install`
* Generate build files with `gulp` command
* Manually copy the files from `src/common/static/imgs` to `public/common/resources/imgs`
* At this point, the site should load automatically in the browser. If this doesn't happen, you can manually open the files in `public/` folder



### How do I sync the files? ###

* First you have to prepare and commit your changes. They are a few steps involved here:
1. Do a `git status` and the `git add .` ( you can also use `git add <file_path>` to include a single file with changes)
2. Then do a `git commit -m "<message describing your changes>"`


* Second part involves bringing new changes to you local files. The command is `git pull origin master`
** Note: At this point, if a file is modified both locally and remote, you will have conflicts and won't be able to proceed until you fix them. If this happens, don't worry and contact me, so we can fix  them together

* Third part involves pushing your work together integrated with the remote changes ( which you brought at step 2 ) to the repository. The command is `git push origin master`

* If needed, recopy the files from `src/common/static/imgs` to `public/common/resources/imgs`