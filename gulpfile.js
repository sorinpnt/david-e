"use strict";
var gulp = require("gulp");
var sass = require("gulp-sass");
var pug = require("gulp-pug");
var uglify = require('gulp-uglify');
var bourbon = require("bourbon").includePaths;
var copy = require('copy');
var cleanCSS = require('gulp-clean-css');
var browserSync = require("browser-sync");
var concat = require('gulp-concat');
var debug = require('gulp-debug');

var buildFolder = 'public/';
var sourceFolder = 'src/';

browserSync.init({ server: { baseDir: "./" + buildFolder } });

gulp.task('compilePug', function() {
	return gulp.src([sourceFolder + '*.pug', '!' + sourceFolder + 'common/**/*.pug'])
			   .pipe(pug({pretty: true}))
			   .pipe(gulp.dest(buildFolder))
		       .pipe(browserSync.reload({ stream: true }));
});

gulp.task('compileCommonSass', function() {
	return gulp.src([ sourceFolder + 'common/style/*.scss'])
	           .pipe(concat('style.css'))
		       .pipe(sass({ includePaths: bourbon }))
               .pipe(gulp.dest(buildFolder + 'common/style/'))
		       .pipe(browserSync.reload({ stream: true }));
});

gulp.task('compileIndexSass', function() {
	return gulp.src([
			sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
			sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
			sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
			sourceFolder + 'common/vendors/yamm3.css',
			sourceFolder + 'common/style/*.scss', 
			sourceFolder + 'index/style/*.scss'
			])
		       .pipe(sass({ includePaths: bourbon }))
	           .pipe(concat('style.css'))
	       	   .pipe(cleanCSS({processImport: false}))
               .pipe(gulp.dest(buildFolder + 'index/style/'))
		       .pipe(browserSync.reload({ stream: true }));
});

gulp.task('compileCommonJs', function() {
	return gulp.src([ sourceFolder + 'common/**/*.js', '!' + sourceFolder + 'common/vendors/**/*.js'])
	           .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'common/js'))
	           .pipe(browserSync.reload({ stream: true }));
});

var indexFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js',
	sourceFolder + 'common/js/**/*.js',
	'!' + sourceFolder + 'common/map_init.js',
	sourceFolder + 'index/js/home-controller.js',
	sourceFolder + 'index/js/app.js'
	];

gulp.task('compileIndexJs', function() {
	return gulp.src( indexFiles )
	       	   .pipe(uglify())
	       	   .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'index/js/'))
	           .pipe(browserSync.reload({ stream: true }));
});

var clientFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js',
	sourceFolder + 'common/js/**/*.js',
	'!' + sourceFolder + 'common/map_init.js',
	sourceFolder + 'client/js/client-controller.js',
	sourceFolder + 'client/js/app.js'
	];
gulp.task('compileClientJs', function() {
	return gulp.src( clientFiles )
	       	   .pipe(uglify())
	       	   .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'client/js/'))
	           .pipe(browserSync.reload({ stream: true }));
});
var categoryFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js',
	sourceFolder + 'common/js/**/*.js',
	'!' + sourceFolder + 'common/map_init.js',
	sourceFolder + 'category/js/category-controller.js',
	sourceFolder + 'category/js/app.js'
	];
gulp.task('compileCategoryJs', function() {
	return gulp.src( categoryFiles )
	       	   .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'category/js/'))
	           .pipe(browserSync.reload({ stream: true }));
});
var contactFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js',
	sourceFolder + 'common/js/**/*.js',
	'!' + sourceFolder + 'common/map_init.js',
	sourceFolder + 'contact/js/contact-controller.js',
	sourceFolder + 'contact/js/app.js'
	];
gulp.task('compileContactJs', function() {
	return gulp.src( contactFiles )
	       	   .pipe(uglify())
	       	   .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'contact/js/'))
	           .pipe(browserSync.reload({ stream: true }));
});
var vendorFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js'
];
gulp.task('compileVendorJs', function() {
	return gulp.src( vendorFiles )
				.pipe(uglify())
	       	   .pipe(concat('vendors.js'))
	           .pipe(gulp.dest(buildFolder + 'common/vendors'))
	           .pipe(browserSync.reload({ stream: true }));
});
var contentFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js',
	sourceFolder + 'common/js/**/*.js',
	'!' + sourceFolder + 'common/map_init.js',
	sourceFolder + 'content/js/content-controller.js',
	sourceFolder + 'content/js/app.js'
];
gulp.task('compileContentJs', function() {
	return gulp.src( contentFiles )
				.pipe(uglify())
	       	    .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'content/js'))
	           .pipe(browserSync.reload({ stream: true }));
});
var salesFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js',
	sourceFolder + 'common/js/**/*.js',
	'!' + sourceFolder + 'common/map_init.js',
	sourceFolder + 'sales/js/sales-controller.js',
	sourceFolder + 'sales/js/app.js'
];
gulp.task('compileSalesJs', function() {
	return gulp.src( salesFiles )
				.pipe(uglify())
	       	   .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'sales/js'))
	           .pipe(browserSync.reload({ stream: true }));
});
var couponFiles = [
	sourceFolder + 'common/vendors/jquery-2.2.4.js',
	sourceFolder + 'common/vendors/angular-js-1.5.6.js',
	sourceFolder + 'common/vendors/angular-animate-1.5.6.js',
	sourceFolder + 'common/vendors/angular-ui-bootstrap-tpls-1.3.3.js',
	sourceFolder + 'common/vendors/bootstrap-3.3.6.js',
	sourceFolder + 'common/vendors/is-on-screen.js',
	sourceFolder + 'common/js/**/*.js',
	'!' + sourceFolder + 'common/map_init.js',
	sourceFolder + 'coupon/js/coupon-controller.js',
	sourceFolder + 'coupon/js/app.js'
];
gulp.task('compileCouponJs', function() {
	return gulp.src( couponFiles )
				.pipe(uglify())
	       	   .pipe(concat('app.js'))
	           .pipe(gulp.dest(buildFolder + 'coupon/js'))
	           .pipe(browserSync.reload({ stream: true }));
});
gulp.task('compileClientScss', function() {
	return gulp.src( [
				sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
				sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
				sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
				sourceFolder + 'common/vendors/yamm3.css',
				sourceFolder + 'common/style/*.scss',
				sourceFolder + 'client/style/**/*.scss'])
		       .pipe(sass({ includePaths: bourbon }))
	       	   .pipe(concat('style.css'))
	       	   .pipe(cleanCSS({processImport: false}))
	           .pipe(gulp.dest(buildFolder + 'client/css'))
	           .pipe(browserSync.reload({ stream: true }));
});
gulp.task('compileContactScss', function() {
	return gulp.src( [
			sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
			sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
			sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
			sourceFolder + 'common/vendors/yamm3.css',
			sourceFolder + 'common/style/*.scss',
			sourceFolder + 'contact/style/**/*.scss' ])
		       .pipe(sass({ includePaths: bourbon }))
	       	   .pipe(concat('style.css'))
	       	   .pipe(cleanCSS({processImport: false}))
	           .pipe(gulp.dest(buildFolder + 'contact/css'))
	           .pipe(browserSync.reload({ stream: true }));
});
gulp.task('compileCouponScss', function() {
	return gulp.src( [
			sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
			sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
			sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
			sourceFolder + 'common/vendors/yamm3.css',
			sourceFolder + 'common/style/*.scss',
			sourceFolder + 'coupon/style/**/*.scss'] )
		       .pipe(sass({ includePaths: bourbon }))
	       	   .pipe(concat('style.css'))
	       	   .pipe(cleanCSS({processImport: false}))
	           .pipe(gulp.dest(buildFolder + 'coupon/css'))
	           .pipe(browserSync.reload({ stream: true }));
});
gulp.task('compileSalesScss', function() {
	return gulp.src( [
			sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
			sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
			sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
			sourceFolder + 'common/vendors/yamm3.css',
			sourceFolder + 'common/style/*.scss',
			sourceFolder + 'sales/style/**/*.scss'] )
		       .pipe(sass({ includePaths: bourbon }))
	       	   .pipe(concat('style.css'))
	       	   .pipe(cleanCSS({processImport: false}))
	           .pipe(gulp.dest(buildFolder + 'sales/css'))
	           .pipe(browserSync.reload({ stream: true }));
});
gulp.task('compileCategoryScss', function() {
	return gulp.src( [
				sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
				sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
				sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
				sourceFolder + 'common/vendors/yamm3.css',
				sourceFolder + 'common/style/*.scss',
				sourceFolder + 'category/style/**/*.scss'] )
		       .pipe(sass({ includePaths: bourbon }))
	       	   .pipe(concat('style.css'))
	       	   .pipe(cleanCSS({processImport: false}))
	           .pipe(gulp.dest(buildFolder + 'category/css'))
	           .pipe(browserSync.reload({ stream: true }));
});
gulp.task('compileContentScss', function() {
	return gulp.src( [
			sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
			sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
			sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
			sourceFolder + 'common/vendors/yamm3.css',
			sourceFolder + 'common/style/*.scss',
			sourceFolder + 'content/style/**/*.scss'])
		       .pipe(sass({ includePaths: bourbon }))
	       	   .pipe(concat('style.css'))
	       	   .pipe(cleanCSS({processImport: false}))
	           .pipe(gulp.dest(buildFolder + 'content/css'))
	           .pipe(browserSync.reload({ stream: true }));
});
var vendorCss = [
	sourceFolder + 'common/vendors/bootstrap-3.3.6.css',
	sourceFolder + 'common/vendors/font-awesome-4.6.3.css',
	sourceFolder + 'common/vendors/bootstrap-rtl-3.2.0-rc2.css',
	sourceFolder + 'common/vendors/yamm3.css'
];
gulp.task('compileVendorCss', function() {
	return gulp.src( vendorCss )
	       	   .pipe(concat('vendors.css'))
	           .pipe(gulp.dest(buildFolder + 'common/vendors'))
	           .pipe(browserSync.reload({ stream: true }));
});
gulp.task('watchPug', function() {
	gulp.watch(sourceFolder + "**/*.pug", ['compilePug']);
});

gulp.task('watchScss', function() {
	gulp.watch(sourceFolder + '*/style/*.scss', [
		'compileCommonSass',
		'compileContactScss',
		'compileIndexSass',
		'compileClientScss',
		'compileCategoryScss',
		'compileContentScss',
		'compileCouponScss',
		'compileSalesScss'
	])
});

gulp.task('watchCommonJs', function() {
	gulp.watch(sourceFolder + 'common/**/*.js', ['compileCommonJs']);
});
gulp.task('watchClientJs', function() {
	gulp.watch(clientFiles, ['compileClientJs']);
});
gulp.task('watchSalesJs', function() {
	gulp.watch(salesFiles, ['compileSalesJs']);
});

gulp.task('watchIndexJs', function() {
	gulp.watch(indexFiles, ['compileIndexJs']);
});

gulp.task('watchContentJs', function() {
	gulp.watch(contentFiles, ['compileContentJs']);
});

gulp.task('watchCategoryJs', function() {
	gulp.watch(categoryFiles, ['compileCategoryJs']);
});

gulp.task('watchContactJs', function() {
	gulp.watch(contactFiles, ['compileContactJs']);
});
gulp.task('watchCouponJs', function() {
	gulp.watch(couponFiles, ['compileCouponJs']);
});
gulp.task('copyCommonImgs', function(cb) {
	copy(sourceFolder + 'common/imgs/*', buildFolder + 'common/imgs', cb);
});

gulp.task('copyCategoryImgs', function(cb) {
	copy(sourceFolder + 'category/imgs/*', buildFolder + 'category/imgs', cb);
});
gulp.task('copyClientImgs', function(cb) {
	copy(sourceFolder + 'client/imgs/*', buildFolder + 'client/imgs', cb);
});
gulp.task('copyIndexImgs', function(cb) {
	copy(sourceFolder + 'index/imgs/*', buildFolder + 'index/imgs', cb);
});

gulp.task('copyFonts', function(cb) {
	copy(sourceFolder + 'common/fonts/*', buildFolder + 'common/fonts', cb);
});
gulp.task('copyContactImgs', function(cb) {
	copy(sourceFolder + 'contact/imgs/*', buildFolder + 'contact/imgs', cb);
});
gulp.task('copyContentImgs', function(cb) {
	copy(sourceFolder + 'content/imgs/*', buildFolder + 'content/imgs', cb);
});
gulp.task('copySalesImgs', function(cb) {
	copy(sourceFolder + 'sales/imgs/*', buildFolder + 'sales/imgs', cb);
});
gulp.task('copyCouponImgs', function(cb) {
	copy(sourceFolder + 'coupon/imgs/*', buildFolder + 'coupon/imgs', cb);
});

gulp.task('copyInitMap', function() {
	return gulp.src( sourceFolder + 'common/js/map_init.js' )
	           .pipe(gulp.dest(buildFolder + 'common/js/'));
});
gulp.task('default', [
	'copyContactImgs',
	'compilePug',
	'copyInitMap',
	'copyCouponImgs',
	'compileCommonSass',
	'compileIndexSass',
	'copyFonts',
	'copyClientImgs',
	'watchCouponJs',
	'copyCategoryImgs',
	'compileCommonJs',
	'compileClientScss',
	"compileContactScss",
	'compileIndexJs',
	'compileCategoryJs',
	'compileCouponScss',
	'copyCommonImgs',
	'compileContactJs',
	'copyIndexImgs',
	'compileSalesJs',
	'copyContentImgs',
	'copySalesImgs',
	'compileContentJs',
	'compileVendorJs',
	'compileContentScss',
	'compileVendorCss',
	'compileClientJs',
	'watchPug',
	'compileCouponJs',
	'watchScss',
	'watchCategoryJs',
	'watchContactJs',
	'watchContentJs',
	'compileCategoryScss',
	'watchCommonJs',
	'watchIndexJs',
	'watchClientJs',
	'compileSalesScss',
	'watchSalesJs'
]);