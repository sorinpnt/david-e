var appCommon = angular.module('app.common', [ 'app.header', 'app.slider' ]);

var stickyNav = function ($window) {  
  function stickyNavLink(scope, element) {
    
    var nav = $(element);
    var targetElement = undefined;
    var windowEl = angular.element($window);

    var shouldBeSticky = function ( scrollTop ) {
        var elementOffset = targetElement.offset().top;
        return  elementOffset - scrollTop <= 0 ;
    };

    var fnResizeOrScroll = function() {
        if ( shouldBeSticky( windowEl.scrollTop() ) ) {
            nav.css('top', 0);
        } else {
            nav.css('top', '-' + nav.height() + 'px');
        }
    };

    windowEl.bind('load', function() {
        targetElement = $("#stickyNavTarget");
        fnResizeOrScroll();
        windowEl.bind('resize', function ( $event ) { fnResizeOrScroll(); } );
        windowEl.bind('scroll', function ( $event ) { fnResizeOrScroll(); } );
    });

  };

  return {
    restrict: 'A',
    link: stickyNavLink
  };
};

stickyNav.$inject = [ '$window' ];
appCommon.directive("stickyNav", stickyNav);

stickyNav.$inject = [ '$window' ];
appCommon.directive("stickyNav", stickyNav);

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = elem.offset().top;
    var elemBottom = elemTop + elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

var stickyBottom = function ($window, $timeout){  
  function stickyBottomLink(scope, element){
    
    var nav = $(element);
    var maxTopDistance = 0;
    var windowEl = angular.element($window);
    var referenceElement = undefined;
    var windowWidth = windowEl.width();
    var stickyLeft = $('#stickyLeft');
    
    var stickyLeftWidth;
    var stickyLeftOffset;
    var stickyLeftHeight;

    var shouldBeRelative = function ()  {
        return windowEl.scrollTop() < referenceElement.offset().top + stickyLeftHeight - windowEl.height()
    }
        
    var fnResize = function() {
        referenceElement = $('#' + scope.stickyReference );
        nav.removeAttr('style');
        stickyLeft = $('#stickyLeft');
        stickyLeftWidth = stickyLeft.width();
        stickyLeftOffset = stickyLeft.offset().left;
        stickyLeftHeight = stickyLeft.height();
        windowWidth = windowEl.outerWidth();
    }
    var fnScroll = function($event) {
        if ( windowWidth >= 1183 && referenceElement.length) {
            if ( shouldBeRelative() ) {
                nav.removeAttr('style');
                stickyLeft.css('position', 'relative' );
            } else {
                nav.removeAttr('style');
                stickyLeft.css('left',  stickyLeftOffset);
                stickyLeft.css('width', stickyLeftWidth );
                stickyLeft.css('position', 'fixed' );
                if ( windowEl.scrollTop() + windowEl.height() < referenceElement.offset().top + referenceElement.height() ) {
                    stickyLeft.css('bottom', '2px');
                } else {
                    stickyLeft.css('bottom', ( $(document).height() - referenceElement.height() - referenceElement.offset().top ) + 'px')
                }
            }
        } else {
            stickyLeft.removeAttr('style');
        }
    }

    windowEl.bind('load', function() {
        referenceElement = $('#' + scope.stickyReference );
        windowEl.on('scroll', fnScroll);
        windowEl.bind('resize', function() {
            fnResize();
            fnScroll();
        });
        fnResize();
        fnScroll();
    });

  };

  return {
    restrict: 'A',
    link: stickyBottomLink,
    scope: {
        stickyReference: '='
    }
  };
};

stickyBottom.$inject = [ '$window', '$timeout' ];
appCommon.directive("stickyBottom", stickyBottom);

var stickyBottomSales = function ($window, $timeout){  
  function stickyBottomSalesLink(scope, element){
    
    var nav = $(element);
    var maxTopDistance = 0;
    var windowEl = angular.element($window);
    var referenceElement = undefined;
    var windowWidth = windowEl.width();
    var stickyLeft = $('#stickyLeft');
    
    var stickyLeftWidth;
    var stickyLeftOffset;
    var stickyLeftHeight;

    var shouldBeRelative = function ()  {
        return  windowEl.height()  - windowEl.scrollTop() > referenceElement.offset().top + stickyLeftHeight 
    }
        
    var fnResize = function() {
        referenceElement = $('#' + scope.stickyReference );
        nav.removeAttr('style');
        stickyLeft = $('#stickyLeft');
        stickyLeftWidth = stickyLeft.width();
        stickyLeftOffset = stickyLeft.offset().left;
        stickyLeftHeight = stickyLeft.height();
        windowWidth = windowEl.outerWidth();
    }
    var fnScroll = function($event) {
        if ( windowWidth >= 1183 && referenceElement.length) {
            if ( shouldBeRelative() ) {
                nav.removeAttr('style');
                stickyLeft.css('position', 'relative' );
            } else {
                nav.removeAttr('style');
                stickyLeft.css('left',  stickyLeftOffset);
                stickyLeft.css('width', stickyLeftWidth );
                stickyLeft.css('position', 'fixed' );
                stickyLeft.css('bottom', '136px')
            }
        } else {
            stickyLeft.removeAttr('style');
        }
    }

    windowEl.bind('load', function() {
        referenceElement = $('#' + scope.stickyReference );
        windowEl.on('scroll', fnScroll);
        windowEl.bind('resize', function() {
            fnResize();
            fnScroll();
        });
        fnResize();
        fnScroll();
    });

  };

  return {
    restrict: 'A',
    link: stickyBottomSalesLink,
    scope: {
        stickyReference: '='
    }
  };
};

stickyBottomSales.$inject = [ '$window', '$timeout' ];
appCommon.directive("stickyBottomSales", stickyBottomSales);


var stickyBottomMd = function ($window, $timeout){  
  function stickyBottomMdLink(scope, element){
    
    var nav = $(element);
    var maxTopDistance = 0;
    var windowEl = angular.element($window);
    var referenceElement = undefined;
    var minWindowsWidth = 0;
    var stickyLeft = $('#stickyLeftMd');
    
    var stickyLeftWidth;
    var stickyLeftOffset;
    var stickyLeftHeight;
    var windowWidth;

    var shouldBeRelative = function ()  {
        return windowEl.scrollTop() < referenceElement.offset().top + stickyLeftHeight - windowEl.height()
    }
        
    var fnResize = function() {
        stickyLeft = $('#stickyLeftMd');
        stickyLeftWidth = stickyLeft.width();
        stickyLeftOffset = stickyLeft.offset().left;
        stickyLeftHeight = stickyLeft.height();
        windowWidth = windowEl.width();
    }
    var fnScroll = function($event) {
        if ( windowWidth >= 975) {
            if ( shouldBeRelative() ) {
                nav.removeAttr('style');
                stickyLeft.css('position', 'relative' );
            } else {
                nav.removeAttr('style');
                stickyLeft.css('left',  stickyLeftOffset + 'px');
                stickyLeft.css('width', stickyLeftWidth );
                stickyLeft.css('position', 'fixed' );
                if ( windowEl.scrollTop() + windowEl.height() < referenceElement.offset().top + referenceElement.height() ) {
                    stickyLeft.css('bottom', '2px');
                } else {
                    stickyLeft.css('bottom', ( $(document).height() - referenceElement.height() - referenceElement.offset().top ) + 'px')
                }
            }
        } else {
            nav.removeAttr('style');
        }
    }

    windowEl.bind('load', function() {
        referenceElement = $('#' + scope.stickyReference );
        windowEl.on('scroll', fnScroll);
        windowEl.bind('resize', function() {
            fnResize();
            fnScroll();
        });
        fnResize();
        fnScroll();
    });

  };

  return {
    restrict: 'A',
    link: stickyBottomMdLink,
    scope: {
        stickyReference: '='
    }
  };
};

stickyBottomMd.$inject = [ '$window', '$timeout' ];
appCommon.directive("stickyBottomMd", stickyBottomMd);


var videoController = function($rootScope, $scope) {
    var ctrl = this;
    $rootScope.modalOpen = false;
    ctrl.playVideo = function(videoId){
        $('#' + videoId)[0]
            .contentWindow
            .postMessage('{"event":"command","func":"' + 'startVideo' + '","args":""}', '*');
    }
    ctrl.closeModal = function() {
        $rootScope.modalOpen = false;
    }
};

videoController.$inject = [ '$rootScope', '$scope'];


appCommon.controller('videoController', videoController);
// map functions
