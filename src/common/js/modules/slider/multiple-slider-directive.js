var multipleSliderTemplate =  '<div class="multiple multiple-slider-container" ng-style="sliderSize">'
    multipleSliderTemplate += '    <div class="image-controls" ng-style="imageControlsSize">'
    multipleSliderTemplate += '        <div class="slide-control semi-circle prev-slide" ng-click="prevSlide()">'
    multipleSliderTemplate += '            <i class="fa directional-arrow fa-chevron-left"></i>'
    multipleSliderTemplate += '        </div>'
    multipleSliderTemplate += '        <div class="slide-control semi-circle next-slide" ng-click="nextSlide()">'
    multipleSliderTemplate += '            <i class="fa directional-arrow fa-chevron-right"></i>'
    multipleSliderTemplate += '        </div>'
    multipleSliderTemplate += '    </div>'
    multipleSliderTemplate += '    <div class="image-frame center-block" ng-style="frameSize">'
    multipleSliderTemplate += '        <div class="image-strip" ng-style="imageStripStyle">'
    multipleSliderTemplate += '            <a href="client.html" class="slide text-center" ng-style="slideStyle" ng-repeat="slide in imageSrc">'
    multipleSliderTemplate += '                 <img class="slide" alt="xxx"'
    multipleSliderTemplate += '                 ng-style="imageStyle"'
    multipleSliderTemplate += '                 ng-src="{{slide.src}}">'
    multipleSliderTemplate += '                  <h5 style="slideTextStyle">{{slide.description}}</h5>'
    multipleSliderTemplate += '             </a>'
    multipleSliderTemplate += '        </div>'
    multipleSliderTemplate += '    </div>'
    multipleSliderTemplate += '</div>';


var multipleSlider = function( $window, $timeout, $compile ) {
	return {
		restrict: 'A',
        template: multipleSliderTemplate,
		scope: {
			imageSrc: '=multipleSlider'
		},
    	link: {
           post: function(scope, element, attr) {
            var imageHeight;
            var imageWidth;
            var totalPictures;
            var picturesPerView;
            var totalScreens;
            var pxToMove;
            scope.sliderSize = {};
            scope.frameSize = {};
            scope.currentIndex = 0;

            var calculateDimensions = function() {
                scope.currentIndex = 0;
                var columnSection = element.parent().parent();
                var totalVisibleSpace = columnSection.width();
                totalPictures = scope.imageSrc.length;
                picturesPerView = parseInt( totalVisibleSpace / imageWidth );
                var picturesDistance = (  totalVisibleSpace - picturesPerView * imageWidth ) / ( picturesPerView * 2 );
                scope.sliderSize = {
                    height: columnSection.siblings().height() - 50,
                    width: columnSection.width()
                }; 
                scope.slideStyle = {
                    "margin-left": picturesDistance,
                    "margin-right": picturesDistance,
                    height: scope.sliderSize.height
                };
                scope.frameSize = {
                    height: scope.sliderSize.height,
                    width: scope.sliderSize.width,
                }

                scope.imageStripStyle = {
                    width: (totalPictures + 1 ) * imageWidth +  parseInt( picturesDistance ) * 2 * ( totalPictures + 1 ),
                    left: 0
                };
                totalScreens =  Math.round( totalPictures / picturesPerView);
                if ( totalPictures === picturesPerView ) {
                    totalScreens = 1;
                }
                scope.imageControlsSize = scope.sliderSize;
                scope.imageControlsSize.top = scope.imageControlsSize.height / 2 - 25;
                pxToMove = picturesDistance * 2 + imageWidth;
                scope.imageStyle = { 
                    height: scope.imageControlsSize.height - 55,
                    width: imageWidth
                };
                scope.slideTextStyle = {
                    "margin-top": scope.imageStyle.height - 55
                }
                angular.element(element).parent().css('height', scope.imageControlsSize.height )
                scope.$digest();
            };

            var moveToFirstSlide = function() {
                scope.currentIndex = 0;
                scope.imageStripStyle.left = 0;
            };

            var moveToLastSlide = function() {
                var move = pxToMove * picturesPerView * ( totalScreens - 1);
                scope.currentIndex = totalScreens - 1;
                scope.imageStripStyle.left = 0;
                scope.imageStripStyle.left -= move;
            };

            scope.nextSlide = function() {
                var move = pxToMove * picturesPerView;
                if ( scope.currentIndex < totalScreens - 1 )  {
                    scope.imageStripStyle.left -= move;
                    scope.currentIndex += 1;
                } else {
                    moveToFirstSlide();
                }
            };

            scope.prevSlide = function() {
                var move = pxToMove * picturesPerView;
                if ( scope.currentIndex > 0 ) {
                    scope.imageStripStyle.left += move;
                    scope.currentIndex -= 1;
                } else {
                    moveToLastSlide();
                }
            };

            angular.element( $window )
                .bind('resize', function ( $event ) {
                    calculateDimensions();
                })
                .bind('load', function( $event) {
                    
                    var img = new Image();
                    img.src = scope.imageSrc[0].src;
                    img.onload = function() {
                        imageHeight = img.height;
                        imageWidth = img.width;
                        calculateDimensions();
                    };
                });

           },
    	   pre: function(scope, element, attr) {}
        }
  };
};

multipleSlider.$inject = [ '$window', '$timeout', '$compile' ];