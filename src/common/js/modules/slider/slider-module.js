var sliderModule = angular.module('app.slider', []);

sliderModule
	.controller('sliderController', sliderController)
	.directive('responsiveSlider', responsiveSlider)
	.directive('multipleSlider', multipleSlider)
	.directive('fullScreenSlider', fullScreenSlider);