var sliderTemplate =  '<div class="responsive-slider-container" ng-style="responsiveSliderDimensions" ng-mouseenter="stopSlider()"  ng-mouseleave="resumeSlider()">'
    sliderTemplate += '    <div class="image-controls" ng-style="responsiveSliderDimensions" >'
    sliderTemplate += '        <div class="slide-control prev-slide" ng-click="prevSlide()">'
    sliderTemplate += '            <i class="fa fa-3x directional-arrow fa-chevron-circle-left"></i>'
    sliderTemplate += '        </div>'
    sliderTemplate += '        <div class="slide-control next-slide" ng-click="nextSlide()">'
    sliderTemplate += '            <i class="fa fa-3x directional-arrow fa-chevron-circle-right"></i>'
    sliderTemplate += '        </div>'
    sliderTemplate += '        <div class="total-slides">'
    sliderTemplate += '            <div class="total-slides-list text-center" >'
    sliderTemplate += '                <div class="slide-list-item" ng-repeat="img in imageSrc track by $index">'
    sliderTemplate += '                    <i  class="fa fa-circle-o"'
    sliderTemplate += '                    ng-class="{true: \'fa-circle\', false: \'fa-circle-o\'}[$index== 3 - currentIndex]"'
    sliderTemplate += '                    ng-click="activateSlide(imageSrc.length - $index - 1)"></i>'
    sliderTemplate += '                </div>'
    sliderTemplate += '            </div>'
    sliderTemplate += '        </div>'
    sliderTemplate += '    </div>'
    sliderTemplate += '    <div class="image-frame" ng-style="responsiveSliderDimensions">'
    sliderTemplate += '        <div class="fb-like-button" ng-if="!autoStart">לייק</div>'
    sliderTemplate += '        <div class="image-strip" ng-style="imageStripStyle">'
    sliderTemplate += '            <img class="slide" alt="xxx"'
    sliderTemplate += '            ng-style="responsiveSliderDimensions"'
    sliderTemplate += '            ng-repeat="slide in imageSrc" '
    sliderTemplate += '            ng-src="{{slide.src}}">'
    sliderTemplate += '        </div>'
    sliderTemplate += '    </div>'
    sliderTemplate += '</div>';


var responsiveSlider = function( $window, $timeout, $compile ) {
	return {
		restrict: 'A',
        template: sliderTemplate,
		scope: {
			imageSrc: '=responsiveSlider',
            autoStart: '='
		},
    	link: {
           post: function(scope, element, attr) {
            var changeTimer;
            var imageHeight;
            var imageWidth;
            scope.responsiveSliderDimensions = {};
           
            var resizeHeightBasedOnWidth = function () {
                var containerWidth = scope.responsiveSliderDimensions.width;
                var ratio = containerWidth / imageWidth;
                scope.responsiveSliderDimensions.height = imageHeight*ratio;
            }

            scope.calculateDimensions = function() {
                var rowElement = element.parent().parent();
                containerWidth = element.parent().width() + 15;
                if ( scope.autoStart === false ) {
                    containerWidth = element.parent().parent().innerWidth();
                }
                scope.responsiveSliderDimensions.width = containerWidth;
                resizeHeightBasedOnWidth();
                scope.imageStripStyle = {
                    width:  scope.imageSrc.length * scope.responsiveSliderDimensions.width + 'px',
                    left: '0px'
                };
                rowElement.height(scope.responsiveSliderDimensions.height);
                scope.$digest();
              };

            scope.activateSlide = function( slideIndex ) {
                if ( slideIndex === scope.imageSrc.length )  { slideIndex = 0 };
                if ( slideIndex === -1 )  { slideIndex = scope.imageSrc.length - 1; };
                scope.imageStripStyle.left = '-' + ( scope.responsiveSliderDimensions.width  * slideIndex ) + 'px';
                scope.currentIndex = slideIndex;
            };

            scope.nextSlide = function() {
                scope.activateSlide( scope.currentIndex + 1);
            };

            scope.prevSlide = function() {
                scope.activateSlide( scope.currentIndex - 1);
            };

            var startChangeSlideTimer = function() {
                scope.nextSlide();
                $timeout.cancel(changeTimer);
                changeTimer = $timeout(startChangeSlideTimer, 3000);
            };

            scope.stopSlider = function() { 
                if ( scope.autoStart === true )  { $timeout.cancel(changeTimer) };
            };

            scope.resumeSlider = function() { 
                if ( scope.autoStart === true )  { startChangeSlideTimer(); }
            };


            angular.element( $window )
                .bind('resize', function ( $event ) {
                    scope.calculateDimensions();
                })
                .bind('load', function( $event) {
                    var img = new Image();
                    img.onload = function() {
                        imageHeight = img.height;
                        imageWidth = img.width;
                        scope.calculateDimensions();
                        scope.currentIndex = 0;

                        if ( scope.autoStart === true ) {
                        
                            scope.activateSlide(scope.imageSrc.length - 1);
                            startChangeSlideTimer();
                        
                        }
                    };
                    img.src = scope.imageSrc[0].src;
                });

           },
    	   pre: function(scope, element, attr) {}
        }
  };
};

responsiveSlider.$inject = [ '$window', '$timeout', '$compile' ];