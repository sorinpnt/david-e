var fullSreenSliderTemplate =  '<div class="fullscreen fullscreen-slider-container" ng-style="sliderSize">'
    fullSreenSliderTemplate += '    <div class="image-controls" ng-style="imageControlsSize">'
    fullSreenSliderTemplate += '        <div class="slide-control semi-circle prev-slide" ng-click="prevSlide()">'
    fullSreenSliderTemplate += '            <i class="fa directional-arrow fa-chevron-left"></i>'
    fullSreenSliderTemplate += '        </div>'
    fullSreenSliderTemplate += '        <div class="slide-control semi-circle next-slide" ng-click="nextSlide()">'
    fullSreenSliderTemplate += '            <i class="fa directional-arrow fa-chevron-right"></i>'
    fullSreenSliderTemplate += '        </div>'
    fullSreenSliderTemplate += '    </div>'
    fullSreenSliderTemplate += '    <div class="image-frame center-block" ng-style="frameSize">'
    /* fullSreenSliderTemplate += '        <div class="fb-like-button">1פייסבוק לייק</div>' */
    fullSreenSliderTemplate += '        <div class="image-strip" ng-style="imageStripStyle">'
    fullSreenSliderTemplate += '            <div class="slide"  ng-style="slideStyle" ng-repeat="slide in imageSrc">'
    fullSreenSliderTemplate += '                <div class="slide-cover pad-md" ng-init="isActive=false"  ng-class="{ true: \'active\', false: \'inactive\'}[isActive]">'
    fullSreenSliderTemplate += '                    <div class="row"> <div class="col-xs-12"><i class="fa fa-times pull-right close-coupon" ng-click="isActive=false"></i></div></div>'
    fullSreenSliderTemplate += '                    <div class="row">'
    fullSreenSliderTemplate += '                        <div class="col-xs-12">'
    fullSreenSliderTemplate += '                            <div class="form-group">  <input class="form-control" placeholder="שם מלא"/></div>' 
    fullSreenSliderTemplate += '                        </div>'
    fullSreenSliderTemplate += '                    </div>'
    fullSreenSliderTemplate += '                    <div class="row">'
    fullSreenSliderTemplate += '                        <div class="col-xs-12">'
    fullSreenSliderTemplate += '                            <div class="form-group">  <input class="form-control" placeholder="נייד"/></div>' 
    fullSreenSliderTemplate += '                        </div>'
    fullSreenSliderTemplate += '                    </div>'
    fullSreenSliderTemplate += '                    <div class="row">'
    fullSreenSliderTemplate += '                        <div class="col-xs-12">'
    fullSreenSliderTemplate += '                            <div class="form-group">  <a class="btn btn-submit-coupon center-block"> שליחה </a></div>' 
    fullSreenSliderTemplate += '                        </div>'
    fullSreenSliderTemplate += '                    </div>'
    fullSreenSliderTemplate += '                </div>'
    fullSreenSliderTemplate += '                <div class="slide-content" ng-class="{ true: \'inactive\', false: \'active\'}[isActive]">'
    fullSreenSliderTemplate += '                    <div class="row text-right">'
    fullSreenSliderTemplate += '                        <div class="col-xs-6 pad-left-sm">'
    fullSreenSliderTemplate += '                            <img class="slide-img pull-left"'
    fullSreenSliderTemplate += '                            ng-src="{{slide.src}}" alt="xxx">'
    fullSreenSliderTemplate += '                        </div>'
    fullSreenSliderTemplate += '                        <div class="col-xs-6 no-right-pad">'
    fullSreenSliderTemplate += '                            <a href="coupon.html"><h4 class="slide-title no-margin">{{slide.title}}</h4></a>'
    fullSreenSliderTemplate += '                            <p class="slide-description">{{slide.description}}</description>'
    fullSreenSliderTemplate += '                        </div>'
    fullSreenSliderTemplate += '                    </div>'
    fullSreenSliderTemplate += '                    <div class="row action-buttons margin-top-md">'
    fullSreenSliderTemplate += '                        <div class="col-xs-12 no-right-pad ">'
    fullSreenSliderTemplate += '                            <button class="btn btn-phone">'
    fullSreenSliderTemplate += '                               <i class="fa fa-phone pull-left"></i> 072-3333333 '
    fullSreenSliderTemplate += '                            </button>'
    fullSreenSliderTemplate += '                        <button class="btn btn-contact" ng-click="isActive=true;">צרו קשר</button>'
    fullSreenSliderTemplate += '                        </div>'
    fullSreenSliderTemplate += '                    </div>'
    fullSreenSliderTemplate += '                </div>'
    fullSreenSliderTemplate += '             </div>'
    fullSreenSliderTemplate += '        </div>'
    fullSreenSliderTemplate += '    </div>'
    fullSreenSliderTemplate += '</div>';


var fullScreenSlider = function( $window, $timeout, $compile ) {
	return {
		restrict: 'A',
        template: fullSreenSliderTemplate,
		scope: {
			imageSrc: '=fullScreenSlider'
		},
    	link: {
           post: function(scope, element, attr) {
            var slideHeight = 150;
            var slideWidth = 200;
            var totalPictures;
            var picturesPerView;
            var totalScreens;
            var pxToMove;
            scope.sliderSize = {};
            scope.frameSize = {};
            scope.currentIndex = 0;

            var calculateDimensions = function() {
                scope.currentIndex = 0;
                var totalVisibleSpace = element.parent().width();
                element.parent().css('height', slideHeight);
                scope.sliderSize = {
                    width: totalVisibleSpace,
                    height: slideHeight
                }
                totalPictures = scope.imageSrc.length;
                picturesPerView = parseInt( totalVisibleSpace / slideWidth );
                var picturesDistance = (  totalVisibleSpace - picturesPerView * slideWidth ) / ( picturesPerView * 2 );
                
                scope.slideStyle = {
                    "margin-left": picturesDistance,
                    "margin-right": picturesDistance
                };

                scope.frameSize = {
                    height: scope.sliderSize.height,
                    width: scope.sliderSize.width,
                };

                scope.imageStripStyle = {
                    width: ( totalPictures  + 1 ) * slideWidth +  parseInt( picturesDistance ) * 2 *  ( totalPictures  + 1 ),
                    left: 0 
                };
                totalScreens =  Math.round( totalPictures / picturesPerView);
                if ( totalPictures === picturesPerView ) {
                    totalScreens = 0;
                }
                scope.imageControlsSize = { top: '35px' };
                pxToMove = picturesDistance * 2 + slideWidth;
                scope.$digest();
              };

            var moveToFirstSlide = function() {
                scope.currentIndex = 0;
                scope.imageStripStyle.left = 0;
            };

            var moveToLastSlide = function() {
                var move = pxToMove * picturesPerView * ( totalScreens - 1);
                scope.currentIndex = totalScreens - 1;
                scope.imageStripStyle.left = 0;
                scope.imageStripStyle.left -= move;
            };

            scope.nextSlide = function() {
                var move = pxToMove * picturesPerView;
                if ( scope.currentIndex < totalScreens - 1 )  {
                    scope.imageStripStyle.left -= move;
                    scope.currentIndex += 1;
                } else {
                    moveToFirstSlide();
                }
            };

            scope.prevSlide = function() {
                var move = pxToMove * picturesPerView;
                if ( scope.currentIndex > 0 ) {
                    scope.imageStripStyle.left += move;
                    scope.currentIndex -= 1;
                } else {
                    moveToLastSlide();
                }
            };

            angular.element( $window )
                .bind('resize', function ( $event ) {
                    calculateDimensions();
                })
                .bind('load', function( $event) {
                    calculateDimensions();
                });

           },
    	   pre: function(scope, element, attr) {}
        }
  };
};

fullScreenSlider.$inject = [ '$window', '$timeout', '$compile' ];