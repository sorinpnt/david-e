var headerController = function ($rootScope, $scope ) {
	this.isMenuExpanded = false;
	this.searchCriteria = '';
	
	this.search = function( $ev ) {
		if($ev.keyCode === 13 || $ev.which === 13) { 
			console.info('searching for ' + this.searchCriteria);
		}
	}
}

headerController.$inject = ['$rootScope', '$scope'];
