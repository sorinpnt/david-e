var map;

var getPopupHtml = function( popUpInfo ) {
    var shopTitle = popUpInfo.shopTitle;
    var imageUrl = popUpInfo.imageUrl;
    var link = popUpInfo.shopLink;
    var contentHtml =  '<div class="container-fluid margin-sm marker-info">' 
        contentHtml += '  <div class="row no-margin-right">' 
        contentHtml += '    <div class="col-xs-6">'
        contentHtml += '  <div class="row">' 
        contentHtml += '    <h5 class="text-center">'
        contentHtml +=    shopTitle
        contentHtml += '    </h5>'
        contentHtml += '  </div>'
        contentHtml += '  <div class="row">' 
        contentHtml += '    <a class="btn btn-sm btn-primary center-block view-shop-page-link" href="' + link + '">View Page</a>'
        contentHtml += '  </div>'
        contentHtml += '    </div>'
        contentHtml += '    <div class="col-xs-6 no-pad-left">' 
        contentHtml += '      <img class="shop-image" src="' + imageUrl + '"/>'
        contentHtml += '    </div>'
        contentHtml += '  </div>'
        contentHtml += '</div>'
    return contentHtml;
};

var initMap = function() {
    var shopMarkers= [{
        icon: 'common/imgs/icon-dress-map.svg',
        popupInfo: {
            imageUrl : 'common/imgs/client-map-icon.png',
            shopTitle: 'Name of the first shop',
            shopLink: ''
        }
    },
    {
        icon: 'common/imgs/icon-dress-map.svg',
        popupInfo: {
            imageUrl : 'common/imgs/client-map-icon.png',
            shopTitle: 'Name of the second shop',
            shopLink: ''
        }
    }];

    map = new google.maps.Map(document.getElementById('googleMap'), {
      center: {lat:  31.9302051, lng: 34.7960756},
      zoom: 17,
      scrollwheel: false
    });

    var addTestMarkers = function( currentLat, currentLong) {
        for (var i =0; i < shopMarkers.length; i++) {
            shopMarkers[i].position = new google.maps.LatLng( currentLat +  ((i+1) * 0.0001), currentLong - ((i+1) * 0.0035));
            shopMarkers[i].popup = new google.maps.InfoWindow({ content: getPopupHtml(shopMarkers[i].popupInfo) });;
            shopMarkers[i].marker = new google.maps.Marker(shopMarkers[i]);
            shopMarkers[i].marker.setMap(map);
            shopMarkers[i].marker.addListener('click', function() { this.popup.open(map, this); });
        };

    };

    if (navigator.geolocation) {
        var locationSuccess = function( position ) {
            map.setCenter({
                lat: position.coords.latitude,
                lng: position.coords.longitude
            });
            addTestMarkers(position.coords.latitude, position.coords.longitude);
        };
        var locationError = function() {
            addTestMarkers( 31.9302051, 34.7960756);
        };

        var locationOptions = {
          enableHighAccuracy: true,
          timeout: 2000,
          maximumAge: 1
        };
        navigator.geolocation.getCurrentPosition(locationSuccess, locationError, locationOptions);
    };


};