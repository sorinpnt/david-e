var map;

var clientController = function( $scope ) {
	var ctrl = this;

	ctrl.mainSlides = [
		{ src: 'client/imgs/slide1-main-carousel.jpg'},
		{ src: 'client/imgs/slide2-main-carousel.jpg'},
		{ src: 'client/imgs/slide3-main-carousel.jpg'},
		{ src: 'client/imgs/slide4-main-carousel.jpg'}
	];



	ctrl.datePickerOptions = {
		"popup-placement": 'bottom',
	};
	ctrl.datePickerFormat = 'dd-MMMM-yyyy';
	ctrl.selectedDate = '';

	$scope.contactFormOpened = false;
	ctrl.closeContactForm = function( $event ) {
		if ( $($event.target).parent().parent().parent()[0] == $('#stickyContactForm')[0] ) {
			$scope.contactFormOpened = !$scope.contactFormOpened;	
		} else {
			$scope.contactFormOpened = false;
		}
	};
}; 

clientController.$inject = [ '$scope'];
