var map;

var categoryController = function($rootScope, $scope ) {
	var ctrl = this;
	$scope.contactFormOpened = false;
	ctrl.closeContactForm = function( $event ) {
		if ( $($event.target).parent().parent().parent()[0] == $('#stickyContactForm')[0] ) {
			$scope.contactFormOpened = !$scope.contactFormOpened;	
		} else {
			$scope.contactFormOpened = false;
		}
	};
}; 

categoryController.$inject = [ '$rootScope', '$scope' ];
