var couponController = function( $scope ) {
	var ctrl = this;
	$scope.contactFormOpened = false;

	ctrl.closeContactForm = function( $event ) {
		if ( $($event.target).parent().parent().parent()[0] == $('#stickyContactForm')[0] ) {
			$scope.contactFormOpened = !$scope.contactFormOpened;	
		} else {
			$scope.contactFormOpened = false;
		}
	};
	
};

couponController.$inject = [ '$scope' ];