var homeController = function( $scope, $timeout) {
	var ctrl = this;
	
	$timeout(function(){
		$scope.modalOpen = true;
	}, 2000);

	ctrl.mainSlides = [
		{ src: 'index/imgs/slide1-main-carousel.jpg'},
		{ src: 'index/imgs/slide2-main-carousel.jpg'},
		{ src: 'index/imgs/slide3-main-carousel.jpg'},
		{ src: 'index/imgs/slide4-main-carousel.jpg'}
	];

	ctrl.presentationSlides = [
		{ href:'#', src: 'index/imgs/presentation-slide.jpg', description: 'שם הסלון 1' },
		{ href:'#', src: 'index/imgs/presentation-slide1.jpg', description: 'שם הסלון 2' },
		{ href:'#', src: 'index/imgs/presentation-slide2.jpg', description: 'שם הסלון 3' },
		{ href:'#', src: 'index/imgs/presentation-slide3.jpg', description: 'שם הסלון 4' },
		{ href:'#', src: 'index/imgs/presentation-slide4.jpg', description: 'שם הסלון 5' }
	];

	ctrl.categorySlides = [
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'},
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'},
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'},
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'},
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'},
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'},
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'},
		{  src: 'common/imgs/coupon-banner-sm.jpg', title: 'בונוס לזוגות המתחתנים  אצלנו', description: 'סוויטה מקסימה ומפנקת'}	
	];

}; 

homeController.$inject = [ '$scope', '$timeout' ];