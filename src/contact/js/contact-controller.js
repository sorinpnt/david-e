var contactController = function( $scope, $timeout, $rootScope, $window ) {

	var playVideo = function ($event) { $event.target.playVideo(); }
	
	var videoEnded = function ($event) {
		if( $event.data === 0 ) {
			$rootScope.modalOpen = false;
			$rootScope.$apply();
		}
	}

	var createVideoPlayer = function() {
		var player = new YT.Player('youtubeEmbedded', { videoId: '0Bmhjf0rKe8', events: { 
			'onReady': playVideo,
			'onStateChange': videoEnded
		} });
	}

	$($window).on('load', function(){

		$timeout(function(){
			$rootScope.modalOpen = true;
    		createVideoPlayer();
		}, 2000);

	});
};

contactController.$inject = [ '$scope', '$timeout', '$rootScope', '$window'];